#coding=utf-8
import pymysql
import json
import xmltodict

# 定义xml转json的函数
def xml_to_json(xml_str):
    # parse是的xml解析器
    xml_parse = xmltodict.parse(xml_str)
    # json库dumps()是将dict转化成json格式,loads()是将json转化成dict格式。
    # dumps()方法的ident=1,格式化json
    json_str = json.dumps(xml_parse, indent=1)
    return json_str

# json转xml函数
def json_to_xml(json_str):
    # xmltodict库的unparse()json转xml
    # 参数pretty 是格式化xml
    xml_str = xmltodict.unparse(json_str, pretty=1)
    return xml_str

# converse object to dict
def fastest_object_to_dict(obj):
    if not hasattr(obj, '__keylist__'):
        return obj
    data = {}
    fields = obj.__keylist__
    for field in fields:
        val = getattr(obj, field)
        if isinstance(val, list):  # tuple not used
            data[field] = []
            for item in val:
                data[field].append(fastest_object_to_dict(item))
        else:
            data[field] = fastest_object_to_dict(val)
    return data

# 连接数据库，做一些sql操作
def connect_sql(sql, *args):
    conn = pymysql.connect(host='192.168.2.239', user='root', passwd='N0mur@4$99!', db='kpos', port=22108,
                           charset='utf8')
    # 获取数据库中对应时间段的预约记录
    cur = conn.cursor()
    # 执行需要的sql语句
    cur.execute(sql, args)
    conn.commit()

    # 获取数据，fetchone获取一条数据，fetchall获取全部数据
    data = cur.fetchall()
    # 关闭游标
    cur.close()
    # 关闭数据库
    conn.close()
    return data

def delete_item_in_sql(sql, *args):
    conn = pymysql.connect(host='192.168.2.239', user='root', passwd='N0mur@4$99!', db='kpos', port=22108,
                           charset='utf8')
    cur = conn.cursor()
    # 执行需要的sql语句
    cur.execute("SET FOREIGN_KEY_CHECKS = 0;")
    cur.execute(sql, args)
    cur.execute("SET FOREIGN_KEY_CHECKS = 1;")
    conn.commit()
    # 关闭游标
    cur.close()
    # 关闭数据库
    conn.close()

