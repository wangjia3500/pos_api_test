import unittest
import pymysql
from parameterized import parameterized, param
from common.requests_handler import RequestsHandler
from common.log import Logger
from common.common import fastest_object_to_dict
from suds.client import Client
import time
from system import POSSystem

middle = time.strftime("%Y-%m-%d", time.localtime())
STARTtime = middle + ' 00:00:00'
ENDtime = middle + ' 23:59:59'

class RecallTest(unittest.TestCase):
    def setUp(self):
        # 请求类实例化
        self.req = RequestsHandler()
        self.logger = Logger().get_log
        # 登录
        login_url = 'http://192.168.2.239:22080/kpos/webapp/license/clientInstanceLogin'
        payload = {
            "appInstanceName": "HH",
            "appInstanceType": "POS"
        }

        res = self.req.visit('post', login_url, json=payload)
        self.sessionKey = res['sessionKey']


        #员工登录
        dest_url = 'http://192.168.2.239:22080/kpos/ws/kposService?wsdl'
        headers ={"Connection": "keep-alive",
                  "Content-type":"text/xml;charset=UTF-8"}
        self.client = Client(dest_url)
        self.client.set_options(headers=headers)

        createuserApi = {"userAuth": {"userPasscode": "11", "sessionKey": self.sessionKey},}
        userAuth_temp = createuserApi["userAuth"]

        res = self.client.service.ListPrivileges(passcode="11", fetchClockInOutStatus=True, userAuth=userAuth_temp)
        ParsedResponse = fastest_object_to_dict(res)
        self.assertEqual(True, ParsedResponse['Result']['successful'])

        now = int((time.time()) * 1000)
        order_param = {"userAuth": {"userId": "1", "sessionKey": self.sessionKey},
                       "order":{"createTime":now,"callerId":"false","type":"TOGO","status":"ORDERED","currentUserId":"1","userId":"1","taxExempt":"false","numOfGuests":"1","totalPrice":"7.95","totalTips":"0","totalTax":"0.80","roundingAmount":"0","printTicketWhenVoid":"true","orderTax":{"taxId":"1","taxAmount":"0.8"},"discountID":"-1","chargeID":"-1","discount":"0","charge":"0","loyaltyDiscount":"false","subOrders":{"seatNum":"1","orderItems":{"saleItemId":"924","seatId":"1","quantity":"1","originalSalePrice":"7.95","price":"7.95","status":"ORDERED","discount":"0","charge":"0"}}}}
        systemHandle = POSSystem()
        systemHandle.saveOrder(order_param)



    @parameterized.expand([
        param(STARTtime, ENDtime)
    ])
    def test_recall_by_date(self, startTime, endTime):
        '''recall页面按日期查询，举例为今天'''
        resRecallByDate = self.client.service.ListOrdersByDateNumber(startTime=startTime, endTime=endTime,
                                                                     filterByCharge=False, filterByDiscount=False)
        ParsedResonseRecallByCharge = fastest_object_to_dict(resRecallByDate)
        # self.loggper.info(ParsedResponseRecallByCharge['orders'])
        orderlist = ParsedResonseRecallByCharge['orders']
        orderIdList = list()
        for i in range(len(orderlist)):
            orderIdList.append(orderlist[i]['id'])
        orderIdList.sort()
        self.logger.info(orderIdList)
        self.assertEqual(True, ParsedResonseRecallByCharge['Result']['successful'])

        self.logger.info("enter recharge")
        conn = pymysql.connect(host='192.168.2.239', user='root', passwd='N0mur@4$99!', db='kpos', port=22108,
                             charset='utf8')
        # 获取游标
        cur = conn.cursor()
        sql = "select * from order_bill where created_on between %s and %s;"
        cur.execute(sql, (STARTtime, ENDtime))

        # 获取数据，fetchone获取一条数据，fetchall获取全部数据
        data = cur.fetchall()
        orderIdListInSql=list()
        for i in range(len(data)):
            id = data[i][0]
            orderIdListInSql.append(id)
        # 关闭游标
        cur.close()
        # 关闭数据库
        conn.close()
        self.assertEqual(orderIdList, orderIdListInSql)

    @parameterized.expand([
        param('2022-01-01 00:00:00', ENDtime)
    ])
    def test_recall_by_during(self, startTime, endTime):
        '''recall页面时间段查询，查询时间默认为2022-01-01至今天'''
        resRecallByDate = self.client.service.ListOrdersByDateNumber(startTime=startTime, endTime=endTime,
                                                                         filterByCharge=False, filterByDiscount=False)
        ParsedResonseRecallByCharge = fastest_object_to_dict(resRecallByDate)
        # self.loggper.info(ParsedResponseRecallByCharge['orders'])
        orderlist = ParsedResonseRecallByCharge['orders']
        orderIdList = list()
        for i in range(len(orderlist)):
            orderIdList.append(orderlist[i]['id'])
        orderIdList.sort()
        self.logger.info(orderIdList)
        self.assertEqual(True, ParsedResonseRecallByCharge['Result']['successful'])

        self.logger.info("enter recharge")
        conn = pymysql.connect(host='192.168.2.239', user='root', passwd='N0mur@4$99!', db='kpos', port=22108,
                                   charset='utf8')
        # 获取游标
        cur = conn.cursor()
        sql = "select * from order_bill where created_on between %s and %s;"
        cur.execute(sql, ('2022-01-01 00:00:00', ENDtime))

        # 获取数据，fetchone获取一条数据，fetchall获取全部数据
        data = cur.fetchall()
        orderIdListInSql = list()
        for i in range(len(data)):
            id = data[i][0]
            orderIdListInSql.append(id)
        # 关闭游标
        cur.close()
        # 关闭数据库
        conn.close()
        self.assertEqual(orderIdList, orderIdListInSql)

    @parameterized.expand([
        param('2022-01-01 00:00:00', ENDtime,'PAID')
    ])
    def test_recall_by_status(self, startTime, endTime,orderStatus):
        '''recall页面按状态查询，默认状态为已支付'''
        resRecallByDate = self.client.service.ListOrdersByDateNumber(startTime=startTime, endTime=endTime,
                                                                         filterByCharge=False, filterByDiscount=False,
                                                                         orderStatus=orderStatus)
        ParsedResonseRecallByCharge = fastest_object_to_dict(resRecallByDate)
        # self.loggper.info(ParsedResponseRecallByCharge['orders'])
        orderlist = ParsedResonseRecallByCharge['orders']
        orderIdList = list()
        for i in range(len(orderlist)):
            orderIdList.append(orderlist[i]['id'])
        orderIdList.sort()
        self.logger.info(orderIdList)
        self.assertEqual(True, ParsedResonseRecallByCharge['Result']['successful'])

        self.logger.info("enter recharge")
        conn = pymysql.connect(host='192.168.2.239', user='root', passwd='N0mur@4$99!', db='kpos', port=22108,
                                   charset='utf8')
        # 获取游标
        cur = conn.cursor()
        sql = "select * from order_bill where created_on between %s and %s and status in %s;"

        cur.execute(sql, ('2022-01-01 00:00:00', ENDtime, (100, 101)))

        # 获取数据，fetchone获取一条数据，fetchall获取全部数据
        data = cur.fetchall()
        orderIdListInSql = list()
        for i in range(len(data)):
            id = data[i][0]
            orderIdListInSql.append(id)
        # 关闭游标
        cur.close()
        # 关闭数据库
        conn.close()
        self.assertEqual(orderIdList, orderIdListInSql)

    @parameterized.expand([
        param('2022-01-01 00:00:00', ENDtime, 'DINE_IN')
    ])
    def test_recall_by_ordertype(self, startTime, endTime, orderType):
        '''recall页面按订单类型查询，默认类型为Dine_in'''
        resRecallByDate = self.client.service.ListOrdersByDateNumber(startTime=startTime, endTime=endTime,
                                                                     filterByCharge=False, filterByDiscount=False,
                                                                     orderType=orderType)
        ParsedResonseRecallByCharge = fastest_object_to_dict(resRecallByDate)
        # self.loggper.info(ParsedResponseRecallByCharge['orders'])
        orderlist = ParsedResonseRecallByCharge['orders']
        orderIdList = list()
        for i in range(len(orderlist)):
            orderIdList.append(orderlist[i]['id'])
        orderIdList.sort()
        self.logger.info(orderIdList)
        self.assertEqual(True, ParsedResonseRecallByCharge['Result']['successful'])

        self.logger.info("enter recharge")
        conn = pymysql.connect(host='192.168.2.239', user='root', passwd='N0mur@4$99!', db='kpos', port=22108,
                               charset='utf8')
        # 获取游标
        cur = conn.cursor()
        sql = "select * from order_bill where created_on between %s and %s and order_type='DINE_IN';"

        cur.execute(sql, ('2022-01-01 00:00:00', ENDtime))

        # 获取数据，fetchone获取一条数据，fetchall获取全部数据
        data = cur.fetchall()
        orderIdListInSql = list()
        for i in range(len(data)):
            id = data[i][0]
            orderIdListInSql.append(id)
        # 关闭游标
        cur.close()
        # 关闭数据库
        conn.close()
        self.assertEqual(orderIdList, orderIdListInSql)

    @parameterized.expand([
        param('2022-01-01 00:00:00', ENDtime, 'CASH')
    ])
    def test_recall_by_paytype(self, startTime, endTime, paymentType):
        '''recall页面按订单类型查询，默认类型为Dine_in'''
        resRecallByDate = self.client.service.ListOrdersByDateNumber(startTime=startTime, endTime=endTime,
                                                                     filterByCharge=False, filterByDiscount=False,
                                                                     paymentType=paymentType)
        ParsedResonseRecallByCharge = fastest_object_to_dict(resRecallByDate)
        # self.loggper.info(ParsedResponseRecallByCharge['orders'])
        orderlist = ParsedResonseRecallByCharge['orders']
        orderIdList = list()
        for i in range(len(orderlist)):
            orderIdList.append(orderlist[i]['id'])
        orderIdList.sort()
        self.logger.info(orderIdList)
        self.assertEqual(True, ParsedResonseRecallByCharge['Result']['successful'])

        self.logger.info("enter recharge")
        conn = pymysql.connect(host='192.168.2.239', user='root', passwd='N0mur@4$99!', db='kpos', port=22108,
                               charset='utf8')
        # 获取游标
        cur = conn.cursor()
        sql = "select * from order_bill where created_on between %s and %s and order_type='DINE_IN';"

        cur.execute(sql, ('2022-01-01 00:00:00', ENDtime))

        # 获取数据，fetchone获取一条数据，fetchall获取全部数据
        data = cur.fetchall()
        orderIdListInSql = list()
        for i in range(len(data)):
            id = data[i][0]
            orderIdListInSql.append(id)
        # 关闭游标
        cur.close()
        # 关闭数据库
        conn.close()
        self.assertEqual(orderIdList, orderIdListInSql)

    def tearDown(self):
            # 关闭连接
            self.req.close_session()