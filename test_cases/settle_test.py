#coding=utf-8
import unittest
import time
from common.requests_handler import RequestsHandler
from common.log import Logger
from common.common import fastest_object_to_dict
from suds.client import Client

class SettleTest(unittest.TestCase):
    def setUp(self):
        # 请求类实例化
        self.req = RequestsHandler()
        self.logger = Logger().get_log
        # 登录
        login_url = 'http://localhost:22080/kpos/webapp/license/clientInstanceLogin'
        payload = {
            "appInstanceName": "YY",
            "appInstanceType": "POS"
        }

        res = self.req.visit('post', login_url, json=payload)
        self.sessionKey = res['sessionKey']

        #员工登录
        dest_url = 'http://localhost:22080/kpos/ws/kposService?wsdl'
        headers ={"Connection": "keep-alive"}
        self.client = Client(dest_url)
        self.client.set_options(headers=headers)

        createuserApi = {"userAuth": {"userPasscode": "11", "sessionKey": self.sessionKey},}
        userAuth_temp = createuserApi["userAuth"]

        res = self.client.service.ListPrivileges(passcode="11", fetchClockInOutStatus=True, userAuth=userAuth_temp)
        ParsedResponse = fastest_object_to_dict(res)
        self.assertEqual(True, ParsedResponse['Result']['successful'])

    def tearDown(self):
        # 关闭连接
        self.req.close_session()

    def test_creditCard_settle(self):
        """
        信用卡支付
        :return:
        """
        # 新增一个订单，信用卡支付
        now = int((time.time()) * 1000)
        order_param = {"userAuth": {"userId": "1", "sessionKey": self.sessionKey},
                       "order": {"createTime": now, "type": "DINE_IN", "status": "ORDERED",
                                 "currentUserId": "1", "userId": "1", "tableId": 86, "taxExempt": "false",
                                 "numOfGuests": "2", "totalPrice": "7.95", "totalTips": "0", "totalTax": "0.80",
                                 "roundingAmount": "0", "printTicketWhenVoid": "true",
                                 "orderTax": {"taxId": "1", "taxAmount": "0.8"}, "discount": "0", "charge": "0",
                                 "loyaltyDiscount": "false", "subOrders": {"seatNum": "1",
                                                                           "orderItems": {"saleItemId": "924",
                                                                                          "seatId": "1",
                                                                                          "quantity": "1",
                                                                                          "originalSalePrice": "7.95",
                                                                                          "price": "7.95",
                                                                                          "status": "ORDERED",
                                                                                          "discount": "0",
                                                                                          "charge": "0"}}}, }

        order_temp = order_param["order"]
        resOrder = self.client.service.SaveOrder(order=order_temp, userAuth=order_param["userAuth"])
        ParsedResponseOrder = fastest_object_to_dict(resOrder)
        self.logger.info(ParsedResponseOrder)
        orderID = ParsedResponseOrder['Result']['id']

        settleParam = {
            "printPaymentReceipt": "true",
            "merchantCopyOnly": "false",
            "paymentRecord": {
                "userId": "1",
                "orderId": orderID,
                "type": "CREDIT_CARD",
                "amount": "8.75",
                "paidAmount": "8.75",
                "multiplePayments": "false",
                "cardType": "UNKNOWN",
            },
            "transactionDetail": {
                "actionType": "SALE_KEYED",
                "amount": "8.75",
                "cardNumber": "4111111111111111",
                "expirationDate": "923",
                "cardholder": "pp"
            },
            "userAuth": {
                "userId": "1",
                "sessionKey": self.sessionKey
            }
        }

        createuserApi = settleParam
        userAuth_temp = createuserApi["userAuth"]
        payment_temp = createuserApi["paymentRecord"]
        transaction_temp = createuserApi["transactionDetail"]
        res = self.client.service.SavePaymentRecord(printPaymentReceipt=True, merchantCopyOnly=False,
                                                    paymentRecord=payment_temp,userAuth=userAuth_temp,
                                                    transactionDetail=transaction_temp)
        ParsedResponse = fastest_object_to_dict(res)
        self.logger.info(ParsedResponse)
        self.assertEqual(True, ParsedResponse['Result']['successful'])


