#coding=utf-8
import unittest
import pymysql
import time
from parameterized import parameterized, param
from common.requests_handler import RequestsHandler
from common.log import Logger
from common.common import delete_item_in_sql
from common.common import fastest_object_to_dict
from common.common import connect_sql
from suds.client import Client

class ItemTest(unittest.TestCase):

    # class param
    req = RequestsHandler()
    logger = Logger().get_log
    sessionKey = 'test'
    client = 'client'
    groupId = 0
    categoryId = 0

    @classmethod
    def setUpClass(cls) -> None:
        # license 登录
        login_url = 'http://192.168.2.239:22080/kpos/webapp/license/clientInstanceLogin'
        payload = {
            "appInstanceName": "YY",
            "appInstanceType": "POS"
        }

        res = ItemTest.req.visit('post', login_url, json=payload)
        ItemTest.sessionKey = res['sessionKey']

        # 员工登录

        dest_url = 'http://192.168.2.239:22080/kpos/ws/kposService?wsdl'
        headers = {"Connection": "keep-alive"}
        ItemTest.client = Client(dest_url)
        ItemTest.client.set_options(headers=headers)

        createuserApi = {"userAuth": {"userPasscode": "11", "sessionKey": ItemTest.sessionKey}, }
        userAuth_temp = createuserApi["userAuth"]

        res = ItemTest.client.service.ListPrivileges(passcode="11", fetchClockInOutStatus=True, userAuth=userAuth_temp)
        ParsedResponse = fastest_object_to_dict(res)
        ItemTest.logger.info(ParsedResponse)

        # 创建组
        group_url = 'http://192.168.2.239:22080/kpos/webapp/menu/menuGroup'
        payload = {
            "menuId": 1,
            "name": "pos-test-group",
            "restaurantHourIds": [1],
            "appInstanceIds": [226, 227, 229, 230, 235, 243, 246, 247, 248]
        }

        res = ItemTest.req.visit('post', group_url, json=payload)
        ItemTest.groupId = res['group']['id']
        ItemTest.logger.info(ItemTest.groupId)

        # 创建带option的类
        category_url = 'http://192.168.2.239:22080/kpos/webapp/menu/menuCategory'
        payload = {
            "applicableToOrderDiscount": True,
            "applicableToTriggerPromotion": True,
            "discountAllowed": True,
            "menuId": 1,
            "name": "pos-test-category-with-option",
            "nameCh": "带option的类",
            "groupId": ItemTest.groupId,
            "options": [{
                "name": "option-test",
                "price": 1,
                "suboptions": [{
                    "name": "suboption-test",
                    "price": "2"
                }]
            }],
            "taxIds": [1]  # [1]为10%的税
        }

        resCategory = ItemTest.req.visit('post', category_url, json=payload)
        ItemTest.logger.debug(resCategory)
        ItemTest.categoryId = resCategory['menuCategory']['id']


    @classmethod
    def tearDownClass(cls) -> None:
        # 删除新增的资源
        sqlCategory = "delete from menu_category where name='pos-test-category-with-option';"
        sqlGroup = "delete from menu_group where name='pos-test-group';"
        delete_item_in_sql(sqlCategory)
        delete_item_in_sql(sqlGroup)
        # 关闭链接
        ItemTest.req.close_session()

    def test_add_item_with_option_and_suboption(self):
        """
        新增带option和suboption的菜并点餐
        :return:
        """
        # POS-5356 POS-5357
        # 新建菜，菜有option和suboption
        item_url = 'http://192.168.2.239:22080/kpos/webapp/menu/menuSaleItem'
        payload = {
            'groupId': self.groupId,
            "categoryId": self.categoryId,
            "name": "item_with-suboption",
            "price": 30,
            "options": [{
                "name": "option-test",
                "price": 1,
                "printerIds": [2],
                "subOptions": [{
                    "name": "suboption-test",
                    "price": "2",
                    "printerIds": [2],
                }]
            }],
            "printerIds": [2]
        }

        resItem = self.req.visit('post', item_url, json=payload)
        self.logger.debug(resItem)
        self.assertEqual("item_with-suboption", resItem['item']['name'])
        itemId = resItem['item']['id']
        self.logger.debug(itemId)
        optionId = resItem['item']['options'][0]['id']
        self.logger.debug(optionId)
        subOptionId = resItem['item']['options'][0]['subOptions'][0]['id']
        self.logger.debug(subOptionId)

        # 点新增的菜， option和subOption都选
        now = int((time.time()) * 1000)
        # 订单参数
        order_param ={
            "order": {
                "createTime": now,
                "callerId": "false",
                "type": "TOGO",
                "status": "ORDERED",
                "currentUserId": "1",
                "userId": "1",
                "taxExempt": "false",
                "numOfGuests": "1",
                "totalPrice": "13",
                "totalTips": "0",
                "totalTax": "1.30",
                "roundingAmount": "0",
                "printTicketWhenVoid": "true",
                "orderTax": {
                    "taxId": "1",
                    "taxAmount": "1.3"
                },
                "loyaltyDiscount": "false",
                "subOrders": {
                    "seatNum": "1",
                    "orderItems": {
                        "saleItemId": itemId,
                        "seatId": "1",
                        "quantity": "1",
                        "originalSalePrice": "10",
                        "price": "10",
                        "status": "ORDERED",
                        "discount": "0",
                        "charge": "0",
                        "options": {
                            "optionId": optionId,
                            "quantity": "1",
                            "optionType": "ITEM",
                            "price": "1",
                            "subOptions": {
                                "subOptionId": subOptionId,
                                "quantity": "1",
                                "optionType": "ITEM",
                                "price": "2"
                            }
                        }
                    }
                }
            },
            "userAuth": {
                "userId": "1",
                "sessionKey": self.sessionKey
            }
        }

        resSaveOrder = self.client.service.SaveOrder(order=order_param["order"], userAuth=order_param["userAuth"])
        ParsedResponse = fastest_object_to_dict(resSaveOrder)
        self.logger.info(ParsedResponse)
        self.assertEqual(True, ParsedResponse['Result']['successful'])
        orderId_Option= ParsedResponse['Result']['id']

        # 下单，新增的菜加option suboption 和 global option
        now = int((time.time()) * 1000)
        order_with_global_option = {
                "createTime": now,
                "callerId": "false",
                "type": "TOGO",
                "status": "ORDERED",
                "currentUserId": "1",
                "userId": "1",
                "taxExempt": "false",
                "numOfGuests": "1",
                "totalPrice": "15",
                "totalTips": "0",
                "totalTax": "1.50",
                "roundingAmount": "0",
                "printTicketWhenVoid": "true",
                "orderTax": {
                    "taxId": "1",
                    "taxAmount": "1.5"
                },
                "loyaltyDiscount": "false",
                "subOrders": {
                    "seatNum": "1",
                    "orderItems": {
                        "saleItemId": itemId,
                        "seatId": "1",
                        "quantity": "1",
                        "originalSalePrice": "10",
                        "price": "10",
                        "status": "ORDERED",
                        "discount": "0",
                        "charge": "0",
                        "options": [{
                            "optionId": optionId,
                            "quantity": "1",
                            "optionType": "ITEM",
                            "price": "1",
                            "subOptions": {
                                "subOptionId": subOptionId,
                                "quantity": "1",
                                "optionType": "ITEM",
                                "price": "2"
                            }
                        },
                        {
                            "optionId": "3801",
                            "optionName": "Tomato",
                            "quantity": "1",
                            "optionType": "GLOBAL",
                            "price": "2.00"
                        },
                        ]
                    }
                }
        }

        resSaveGlobalOptionOrder = self.client.service.SaveOrder(order=order_with_global_option,
                                                                 userAuth=order_param["userAuth"])
        respGlobalOption = fastest_object_to_dict(resSaveGlobalOptionOrder)
        self.logger.info(respGlobalOption)
        self.assertEqual(True, respGlobalOption['Result']['successful'])
        orderId_globalOption = respGlobalOption['Result']['id']

        # # 删除资源
        sqlOptionOrder = "delete from order_bill where id = %s"
        sqlOption = "delete from menu_item where name='option-test';"
        sqlSubOption = "delete from menu_item where name='suboption-test';"
        sqlItem = "delete from menu_item where name='item_with-suboption';"
        sqlSubOptionInfo = "delete from menu_item_info where name='suboption-test';"
        sqlOptionInfo = "delete from menu_item_info where name='option-test';"
        sqlItemInfo = "delete from menu_item_info where name='item_with-suboption';"

        delete_item_in_sql(sqlOptionOrder, orderId_Option)
        delete_item_in_sql(sqlOptionOrder, orderId_globalOption)
        delete_item_in_sql(sqlSubOption)
        delete_item_in_sql(sqlOption)
        delete_item_in_sql(sqlItem)
        delete_item_in_sql(sqlSubOptionInfo)
        delete_item_in_sql(sqlOptionInfo)
        delete_item_in_sql(sqlItemInfo)

    def test_add_item_with_option(self):
        """
        新增只有一级option的菜并点餐
        :return:
        """
        # POS-5355
        # 新建菜，菜有option
        item_url = 'http://192.168.2.239:22080/kpos/webapp/menu/menuSaleItem'
        payload = {
            'groupId': self.groupId,
            "categoryId": self.categoryId,
            "name": "item_with-option",
            "price": 30,
            "options": [{
                "name": "option-test",
                "price": 1,
                "printerIds": [2],
            }],
            "printerIds": [2]
        }

        resItem = self.req.visit('post', item_url, json=payload)
        self.logger.debug(resItem)
        self.assertEqual("item_with-option", resItem['item']['name'])
        itemId = resItem['item']['id']
        self.logger.debug(itemId)
        optionId = resItem['item']['options'][0]['id']
        self.logger.debug(optionId)

        # 点新增的菜， 选option
        now = int((time.time()) * 1000)
        # 订单参数
        order_param ={
            "order": {
                "createTime": now,
                "callerId": "false",
                "type": "TOGO",
                "status": "ORDERED",
                "currentUserId": "1",
                "userId": "1",
                "taxExempt": "false",
                "numOfGuests": "1",
                "totalPrice": "11",
                "totalTips": "0",
                "totalTax": "1.10",
                "roundingAmount": "0",
                "printTicketWhenVoid": "true",
                "orderTax": {
                    "taxId": "1",
                    "taxAmount": "1.1"
                },
                "loyaltyDiscount": "false",
                "subOrders": {
                    "seatNum": "1",
                    "orderItems": {
                        "saleItemId": itemId,
                        "seatId": "1",
                        "quantity": "1",
                        "originalSalePrice": "10",
                        "price": "10",
                        "status": "ORDERED",
                        "discount": "0",
                        "charge": "0",
                        "options": {
                            "optionId": optionId,
                            "quantity": "1",
                            "optionType": "ITEM",
                            "price": "1",
                        }
                    }
                }
            },
            "userAuth": {
                "userId": "1",
                "sessionKey": self.sessionKey
            }
        }

        resSaveOrder = self.client.service.SaveOrder(order=order_param["order"], userAuth=order_param["userAuth"])
        ParsedResponse = fastest_object_to_dict(resSaveOrder)
        self.logger.info(ParsedResponse)
        self.assertEqual(True, ParsedResponse['Result']['successful'])

        # 删除资源
        sqlOption = "delete from menu_item where name='option-test';"
        sqlItem = "delete from menu_item where name='item_with-option';"
        sqlOptionInfo = "delete from menu_item_info where name='option-test';"
        sqlItemInfo = "delete from menu_item_info where name='item_with-option';"

        delete_item_in_sql(sqlOption)
        delete_item_in_sql(sqlItem)
        delete_item_in_sql(sqlOptionInfo)
        delete_item_in_sql(sqlItemInfo)


    @parameterized.expand([
        param("ALL_YOU_CAN_EAT"),
        param("RECOMMENDED"),
        param("SPICY")
    ])
    def test_add_item_with_label(self, labelParam):
        """
        创建带标签的菜并点餐
        :param labelParam: 标签
        :return:
        """
        # POS-5351 POS-5352 POS-5350
        # 新建菜，菜有标签
        item_url = 'http://192.168.2.239:22080/kpos/webapp/menu/menuSaleItem'
        payload = {
            'groupId': self.groupId,
            "categoryId": self.categoryId,
            "name": "item_with-label",
            "price": 30,
            "printerIds": [2],
            "properties": [{
                "name": labelParam,
                "value": True
            }]
        }

        resItem = self.req.visit('post', item_url, json=payload)
        self.logger.debug(resItem)
        self.assertEqual("item_with-label", resItem['item']['name'])
        itemId = resItem['item']['id']
        self.logger.debug(itemId)

        # 点餐
        now = int((time.time()) * 1000)
        order_param = {
            "order": {
                "createTime": now,
                "callerId": "false",
                "type": "TOGO",
                "status": "ORDERED",
                "currentUserId": "1",
                "userId": "1",
                "taxExempt": "false",
                "numOfGuests": "1",
                "totalPrice": "30",
                "totalTips": "0",
                "totalTax": "3",
                "roundingAmount": "0",
                "printTicketWhenVoid": "true",
                "orderTax": {
                    "taxId": "1",
                    "taxAmount": "3"
                },
                "loyaltyDiscount": "false",
                "subOrders": {
                    "seatNum": "1",
                    "orderItems": {
                        "saleItemId": itemId,
                        "seatId": "1",
                        "quantity": "1",
                        "originalSalePrice": "30",
                        "price": "30",
                        "status": "ORDERED",
                        "discount": "0",
                        "charge": "0",
                    }
                }
            },
            "userAuth": {
                "userId": "1",
                "sessionKey": self.sessionKey
            }
        }

        resSaveOrder = self.client.service.SaveOrder(order=order_param["order"], userAuth=order_param["userAuth"])
        ParsedResponse = fastest_object_to_dict(resSaveOrder)
        self.logger.info(ParsedResponse)
        self.assertEqual(True, ParsedResponse['Result']['successful'])
        orderId = ParsedResponse['Result']['id']

        # 删除资源
        sqlOrder = "delete from order_bill where id = %s;"
        sqlOption = "delete from menu_item where name='option-test';"
        sqlItem = "delete from menu_item where name='item_with-option';"
        sqlOptionInfo = "delete from menu_item_info where name='option-test';"
        sqlItemInfo = "delete from menu_item_info where name='item_with-option';"

        delete_item_in_sql(sqlOrder, orderId)
        delete_item_in_sql(sqlOption)
        delete_item_in_sql(sqlItem)
        delete_item_in_sql(sqlOptionInfo)
        delete_item_in_sql(sqlItemInfo)


