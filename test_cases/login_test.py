#coding=utf-8
import unittest
from parameterized import parameterized, param
from common.requests_handler import RequestsHandler
from common.log import Logger
from common.common import fastest_object_to_dict
from suds.client import Client

class LoginTest(unittest.TestCase):
    def setUp(self):
        # 请求类实例化
        self.req = RequestsHandler()
        self.logger = Logger().get_log
        self.sessionKey = "test"

    def tearDown(self):
        # 关闭连接
        self.req.close_session()

    def test_login_companyProfile(self):
        """
        获取商户信息，包含所用版本信息，license等
        """
        companyProfile_url = "http://localhost:22080/kpos/webapp/store/fetchCompanyProfile"
        res = self.req.visit('get', companyProfile_url)
        self.logger.info(res)
        self.assertEqual(True, res['result']['successful'])
    #
    # param("appInstanceID","name")
    @parameterized.expand([
         param("226","SERVER"),
     ])
    def test_login_findAppInstances(self, id, name):
        dest_url = 'http://localhost:22080/kpos/ws/kposService?wsdl'
        client = Client(dest_url)
        res = client.service.FindAppInstances(id=id, fetchDetails=True, type="POS")
        ParsedResponse = fastest_object_to_dict(res)
        self.logger.info(ParsedResponse)
        self.assertEqual(True, ParsedResponse['Result']['successful'])
        self.assertEqual(name, ParsedResponse['appInstances'][0]['displayName'])

    @parameterized.expand([
        param("SERVER","POS")
    ])
    def test_login_clientInstanceLogin_with_correct_license(self, appInstanceName, appInstanceType):
        """
        appInstanceName: license名称
        appInstanceType: license类型
        """
        login_url = 'http://localhost:22080/kpos/webapp/license/clientInstanceLogin'
        payload = {
            "appInstanceName": appInstanceName,
            "appInstanceType": appInstanceType
        }

        res = self.req.visit('post', login_url, json=payload)

        self.logger.debug(res)
        self.sessionKey = res['sessionKey']
        self.logger.debug(self.sessionKey)
         # 根据返回结果中的msg进行断言
        self.assertEqual(True, res['result']['successful'])

    def test_login_ListPrivileges(self):
        login_url = 'http://localhost:22080/kpos/webapp/license/clientInstanceLogin'
        payload = {
            "appInstanceName": "YY",
            "appInstanceType": "POS"
        }

        res = self.req.visit('post', login_url, json=payload)

        self.logger.debug(res)
        self.sessionKey = res['sessionKey']
        self.logger.debug(self.sessionKey)
         # 根据返回结果中的msg进行断言
        self.assertEqual(True, res['result']['successful'])
        dest_url = 'http://localhost:22080/kpos/ws/kposService?wsdl'

        client = Client(dest_url) # 查看该webservice接口下有包含哪些接口
        createuserApi = {"userAuth": {"userPasscode": "11", "sessionKey": self.sessionKey},}
        userAuth_temp = createuserApi["userAuth"]



        res = client.service.ListPrivileges(passcode="11", fetchClockInOutStatus=True, userAuth=userAuth_temp)
        ParsedResponse = fastest_object_to_dict(res)


        # self.logger.info(ParsedResponse)
        self.assertEqual(True, ParsedResponse['Result']['successful'])


    # # param("appInstanceName","appInstanceType")
    # @parameterized.expand([
    #     param("resetful","EMENU")
    # ])
    # def test_login_clientInstanceLogin_with_instance_out_of_limit(self, appInstanceName, appInstanceType):
    #     """
    #     appInstanceName: license名称
    #     appInstanceType: license类型
    #     """
    #     login_url = 'http://localhost:22080/kpos/webapp/license/clientInstanceLogin'
    #     payload = {
    #         "appInstanceName": appInstanceName,
    #         "appInstanceType": appInstanceType
    #     }
    #
    #     res = self.req.visit('post', login_url, json=payload)
    #     self.logger.debug(res)
    #      # 根据返回结果中的msg进行断言
    #     self.assertEqual(False, res['result']['successful'])
    #     self.assertEqual("Not enough license", res['result']['failureReason'])

if __name__ == '__main__':
    unittest.main()
