#coding=utf-8
import unittest
import time
from system import POSSystem

class SanityTest(unittest.TestCase):

    def test_dinein_order(self):
        # 校验license
        systemHandle = POSSystem()
        systemHandle.login_license()
        # license校验失败直接报错
        self.assertEqual(True, systemHandle.licenseLoginFlag)

        ## 员工登录
        systemHandle.login_ListPrivileges()
        self.assertEqual(True, systemHandle.privilegesFlag)

        # 获取桌子的区域
        systemHandle.listAreas(False)
        systemHandle.logger.debug(systemHandle.areaList[0]['id'])
        areaId = systemHandle.areaList[0]['id']

        # 获取指定区域的桌子
        tableId = systemHandle.listTables(areaId, True)

        ## 保存订单
        now = int((time.time()) * 1000)
        order_param = {"userAuth": {"userId": "1", "sessionKey": systemHandle.sessionKey},
         "order": {"createTime": now, "type": "DINE_IN", "status": "ORDERED",
                   "currentUserId": "1", "userId": "1", "tableId": tableId, "taxExempt": "false",
                   "numOfGuests": "2", "totalPrice": "7.95", "totalTips": "0", "totalTax": "0.80",
                   "roundingAmount": "0", "printTicketWhenVoid": "true",
                   "orderTax": {"taxId": "1", "taxAmount": "0.8"}, "discount": "0", "charge": "0",
                   "loyaltyDiscount": "false", "subOrders": {"seatNum": "1",
                                                             "orderItems": {"saleItemId": "924", "seatId": "1",
                                                                            "quantity": "1",
                                                                            "originalSalePrice": "7.95",
                                                                            "price": "7.95",
                                                                            "status": "ORDERED", "discount": "0",
                                                                            "charge": "0"}}}, }
        systemHandle.saveOrder(order_param)

        #close session
        systemHandle.close_session()
