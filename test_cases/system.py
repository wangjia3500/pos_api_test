#coding=utf-8
from common.requests_handler import RequestsHandler
from common.log import Logger
from common.common import fastest_object_to_dict
from suds.client import Client

class POSSystem:
    def __init__(self):
        self.sessionKey = 'test'
        self.licenseLoginFlag = False # license校验标记位
        self.privilegesFlag = False # 员工登录标记位
        self.req = RequestsHandler()
        self.logger = Logger().get_log
        self.areaList = []

    def login_license(self):
        ## license校验
        login_url = 'http://localhost:22080/kpos/webapp/license/clientInstanceLogin'
        payload = {
            "appInstanceName": "YY",
            "appInstanceType": "POS"
        }
        res = self.req.visit('post', login_url, json=payload)

        self.logger.debug(res)
        self.sessionKey = res['sessionKey']
        self.logger.debug(self.sessionKey)
        if res['result']['successful'] == True:
            self.licenseLoginFlag = True

    def login_ListPrivileges(self):
        dest_url = 'http://localhost:22080/kpos/ws/kposService?wsdl'

        client = Client(dest_url)  # 查看该webservice接口下有包含哪些接口
        createuserApi = {"userAuth": {"userPasscode": "11", "sessionKey": self.sessionKey},}
        userAuth_temp = createuserApi["userAuth"]

        res = client.service.ListPrivileges(passcode="11", fetchClockInOutStatus=True, userAuth=userAuth_temp)
        ParsedResponse = fastest_object_to_dict(res)

        # self.logger.info(ParsedResponse)
        self.privilegesFlag = ParsedResponse['Result']['successful']

    def listAreas(self, fetchOrdersFlag):
        # 列出堂食桌子的区域
        # 保存订单
        dest_url = 'http://localhost:22080/kpos/ws/kposService?wsdl'
        client = Client(dest_url)  # 查看该webservice接口下有包含哪些接口
        createuserApi = {"userAuth": {"userId": "1", "sessionKey": self.sessionKey},}
        userAuth_temp = createuserApi["userAuth"]
        res = client.service.ListAreas(fetchOrders=fetchOrdersFlag, userAuth=userAuth_temp)
        ParsedResponse = fastest_object_to_dict(res)
        # self.logger.info(ParsedResponse['areas'][0]['tables'][0]['id'])
        self.areaList = (ParsedResponse['areas'])

    def listTables(self, areaId, fetchOrdersFlag):
        # 列出指定区域的桌子，并查询桌子上面的订餐信息
        dest_url = 'http://localhost:22080/kpos/ws/kposService?wsdl'
        client = Client(dest_url)  # 查看该webservice接口下有包含哪些接口
        createuserApi = {"userAuth": {"userId": "1", "sessionKey": self.sessionKey},}
        userAuth_temp = createuserApi["userAuth"]
        res = client.service.ListTables(areaId=areaId, fetchOrders=fetchOrdersFlag, userAuth=userAuth_temp)
        ParsedResponse = fastest_object_to_dict(res)
        tablelist = ParsedResponse['table']
        tableId = 0
        self.logger.info((tablelist))
        for i in range(len(tablelist)):
            # if tablelist[i]['status'] =='EMPTY':
            #     tableId = tablelist[i]['id']
            #    break
            if 'orders' not in tablelist[i]:
                tableId = tablelist[i]['id']
                break
        self.logger.info(tableId)
        return tableId

    def saveOrder(self, orderParam):
        # 保存订单
        dest_url = 'http://localhost:22080/kpos/ws/kposService?wsdl'
        client = Client(dest_url)  # 查看该webservice接口下有包含哪些接口
        createuserApi = orderParam
        userAuth_temp = createuserApi["userAuth"]
        order_temp = createuserApi["order"]
        res = client.service.SaveOrder(order=order_temp, userAuth=userAuth_temp)
        ParsedResponse = fastest_object_to_dict(res)
        self.logger.info(ParsedResponse)

    def close_session(self):
        # 关闭会话链接
        self.req.close_session()


