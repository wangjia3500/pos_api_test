#coding=utf-8
import unittest
import time
from common.requests_handler import RequestsHandler
from common.log import Logger
from common.common import fastest_object_to_dict
from suds.client import Client

class CashTest(unittest.TestCase):
    def setUp(self):
        # 请求类实例化
        self.req = RequestsHandler()
        self.logger = Logger().get_log
        # 登录
        login_url = 'http://localhost:22080/kpos/webapp/license/clientInstanceLogin'
        payload = {
            "appInstanceName": "YY",
            "appInstanceType": "POS"
        }

        res = self.req.visit('post', login_url, json=payload)
        self.sessionKey = res['sessionKey']

        #员工登录
        dest_url = 'http://localhost:22080/kpos/ws/kposService?wsdl'
        headers ={"Connection": "keep-alive"}
        self.client = Client(dest_url)
        self.client.set_options(headers=headers)

        createuserApi = {"userAuth": {"userPasscode": "11", "sessionKey": self.sessionKey},}
        userAuth_temp = createuserApi["userAuth"]

        res = self.client.service.ListPrivileges(passcode="11", fetchClockInOutStatus=True, userAuth=userAuth_temp)
        ParsedResponse = fastest_object_to_dict(res)
        self.assertEqual(True, ParsedResponse['Result']['successful'])

    def tearDown(self):
        # 关闭连接
        self.req.close_session()

    def test_1_cashIn(self):
        """
        cash In 现金备款
        :return:
        """

        # pullCashDrawer 开钱箱
        userAuth = {"userId":1, "sessionKey": self.sessionKey}

        resPullCashDrawer = self.client.service.PullCashDrawer(userID="1", userAuth=userAuth)
        ParsedRespPullCashDrawer = fastest_object_to_dict(resPullCashDrawer)
        self.logger.info(ParsedRespPullCashDrawer)
        self.assertEqual(True, ParsedRespPullCashDrawer['successful'])

        # BalanceCashDrawer 获取type 是Cash_in 还是cash_out
        resBalanceCashDrawer = self.client.service.BalanceCashDrawer(passcode="11", userAuth=userAuth)
        ParsedRespBalanceCashDrawer = fastest_object_to_dict(resBalanceCashDrawer)
        self.logger.info(ParsedRespBalanceCashDrawer)
        self.assertEqual(True, ParsedRespBalanceCashDrawer['Result']['successful'])
        self.assertEqual('CASH_IN', ParsedRespBalanceCashDrawer['expectedType'])

        # BalanceCashDrawer 往钱箱放钱
        cashIn_param = {
            "passcode": "11",
            "type": "CASH_IN",
            "amount": "100",
            "cashDetailType": {
                "noteDetails": {
                    "cashNoteDetailType": [
                        {
                            "name": "$100",
                            "count": "1",
                            "amount": "100"
                        },
                        {
                            "name": "$50",
                            "count": "0",
                            "amount": "0"
                        },
                        {
                            "name": "$20",
                            "count": "0",
                            "amount": "0"
                        },
                        {
                            "name": "$10",
                            "count": "0",
                            "amount": "0"
                        },
                        {
                            "name": "$5",
                            "count": "0",
                            "amount": "0"
                        },
                        {
                            "name": "$1",
                            "count": "0",
                            "amount": "0"
                        }
                    ]
                },
                "coinDetails": {
                    "cashCoinDetailType": [
                        {
                            "name": "$2",
                            "count": "0",
                            "roll": "0",
                            "amount": "0"
                        },
                        {
                            "name": "$1",
                            "count": "0",
                            "roll": "0",
                            "amount": "0"
                        },
                        {
                            "name": "$0.25",
                            "count": "0",
                            "roll": "0",
                            "amount": "0"
                        },
                        {
                            "name": "$0.1",
                            "count": "0",
                            "roll": "0",
                            "amount": "0"
                        },
                        {
                            "name": "$0.05",
                            "count": "0",
                            "roll": "0",
                            "amount": "0"
                        },
                        {
                            "name": "$0.01",
                            "count": "0",
                            "roll": "0",
                            "amount": "0"
                        }
                    ]
                }
            }
        }
        resBalanceCashIn = self.client.service.BalanceCashDrawer(passcode=cashIn_param['passcode'], userAuth=userAuth,
                                                                 type=cashIn_param['type'],amount=cashIn_param['amount'])
        ParsedRespBalanceCashIn= fastest_object_to_dict(resBalanceCashIn)
        self.logger.info(ParsedRespBalanceCashIn)
        self.assertEqual(True, ParsedRespBalanceCashIn['Result']['successful'])

    def test_2_cashOut(self):
        """
        cash Out 现金结算
        :return:
        """

        userAuth = {"userId":1, "sessionKey": self.sessionKey}

        # 创建一个现金付款的订单
        now = int((time.time()) * 1000)
        order_param = {
            "order": {
                "createTime": now,
                "callerId": "false",
                "type": "TOGO",
                "status": "ORDERED",
                "currentUserId": "1",
                "userId": "1",
                "taxExempt": "true",
                "numOfGuests": "1",
                "totalPrice": "10",
                "totalTips": "0",
                "totalTax": "0.00",
                "roundingAmount": "0",
                "printTicketWhenVoid": "true",
                "orderTax": {
                    "taxId": "1",
                    "taxAmount": "0"
                },
                "discountID": "-1",
                "chargeID": "-1",
                "discount": "0",
                "charge": "0",
                "loyaltyDiscount": "false",
                "subOrders": {
                    "seatNum": "1",
                    "orderItems": {
                        "saleItemId": "961",
                        "seatId": "1",
                        "quantity": "1",
                        "originalSalePrice": "10",
                        "price": "10",
                        "status": "ORDERED",
                        "discount": "0",
                        "charge": "0"
                    }
                }
            },
        }

        resOrder = self.client.service.SaveOrder(order=order_param['order'], userAuth=userAuth)
        ParsedRespOrder = fastest_object_to_dict(resOrder)
        self.logger.info(ParsedRespOrder)
        self.assertEqual(True, ParsedRespOrder['Result']['successful'])
        orderId = ParsedRespOrder['Result']['id']

        # 对订单现金付款
        payment_param = {
            "printPaymentReceipt": "false",
            "merchantCopyOnly": "false",
            "paymentRecord": {
                "userId": "1",
                "orderId": orderId,
                "type": "CASH",
                "amount": "10",
                "paidAmount": "10",
                "multiplePayments": "false",
                "changeAmount": "0",
                "tipAmount": "0",
            },
        }

        resPay = self.client.service.SavePaymentRecord(paymentRecord=payment_param['paymentRecord'],
                                                         userAuth=userAuth)
        ParsedRespPay = fastest_object_to_dict(resPay)
        self.logger.info(ParsedRespPay)
        self.assertEqual(True, ParsedRespOrder['Result']['successful'])

        # pullCashDrawer 开钱箱
        resPullCashDrawer = self.client.service.PullCashDrawer(userID="1", userAuth=userAuth)
        ParsedRespPullCashDrawer = fastest_object_to_dict(resPullCashDrawer)
        self.logger.info(ParsedRespPullCashDrawer)
        self.assertEqual(True, ParsedRespPullCashDrawer['successful'])

        # BalanceCashDrawer 获取type 是Cash_in 还是cash_out
        resBalanceCashDrawer = self.client.service.BalanceCashDrawer(passcode="11", userAuth=userAuth)
        ParsedRespBalanceCashDrawer = fastest_object_to_dict(resBalanceCashDrawer)
        self.logger.info(ParsedRespBalanceCashDrawer)
        self.assertEqual(True, ParsedRespBalanceCashDrawer['Result']['successful'])
        self.assertEqual('CASH_OUT', ParsedRespBalanceCashDrawer['expectedType'])
        totalCashSale = ParsedRespBalanceCashDrawer['totalCashSale']
        self.assertEqual(10, totalCashSale)

        # BalanceCashDrawer 现金结算
        cashOut_param = {
            "passcode": "11",
            "type": "CASH_OUT",
            "amount": "110",
            "cashDetailType": {
                "noteDetails": {
                    "cashNoteDetailType": [
                        {
                            "name": "$100",
                            "count": "1",
                            "amount": "100"
                        },
                        {
                            "name": "$50",
                            "count": "0",
                            "amount": "0"
                        },
                        {
                            "name": "$20",
                            "count": "0",
                            "amount": "0"
                        },
                        {
                            "name": "$10",
                            "count": "1",
                            "amount": "10"
                        },
                        {
                            "name": "$5",
                            "count": "0",
                            "amount": "0"
                        },
                        {
                            "name": "$1",
                            "count": "0",
                            "amount": "0"
                        }
                    ]
                },
                "coinDetails": {
                    "cashCoinDetailType": [
                        {
                            "name": "$2",
                            "count": "0",
                            "roll": "0",
                            "amount": "0"
                        },
                        {
                            "name": "$1",
                            "count": "0",
                            "roll": "0",
                            "amount": "0"
                        },
                        {
                            "name": "$0.25",
                            "count": "0",
                            "roll": "0",
                            "amount": "0"
                        },
                        {
                            "name": "$0.1",
                            "count": "0",
                            "roll": "0",
                            "amount": "0"
                        },
                        {
                            "name": "$0.05",
                            "count": "0",
                            "roll": "0",
                            "amount": "0"
                        },
                        {
                            "name": "$0.01",
                            "count": "0",
                            "roll": "0",
                            "amount": "0"
                        }
                    ]
                }
            },
        }
        resBalanceCashOut = self.client.service.BalanceCashDrawer(passcode=cashOut_param['passcode'], userAuth=userAuth,
                                                                 type=cashOut_param['type'],amount=cashOut_param['amount'])
        ParsedRespBalanceCashOut= fastest_object_to_dict(resBalanceCashOut)
        self.logger.info(ParsedRespBalanceCashOut)
        self.assertEqual(True, ParsedRespBalanceCashOut['Result']['successful'])

    def test_3_cashOut_with_wrong_amount(self):
        # Cash In
        userAuth = {"userId": 1, "sessionKey": self.sessionKey}
        # BalanceCashDrawer 获取type 是Cash_in 还是cash_out
        resBalanceCashDrawer = self.client.service.BalanceCashDrawer(passcode="11", userAuth=userAuth)
        ParsedRespBalanceCashDrawer = fastest_object_to_dict(resBalanceCashDrawer)
        self.logger.info(ParsedRespBalanceCashDrawer)
        self.assertEqual(True, ParsedRespBalanceCashDrawer['Result']['successful'])
        self.assertEqual('CASH_IN', ParsedRespBalanceCashDrawer['expectedType'])

        # BalanceCashDrawer 往钱箱放钱
        cashIn_param = {
            "passcode": "11",
            "type": "CASH_IN",
            "amount": "100",
            "cashDetailType": {
                "noteDetails": {
                    "cashNoteDetailType": [
                        {
                            "name": "$100",
                            "count": "1",
                            "amount": "100"
                        },
                        {
                            "name": "$50",
                            "count": "0",
                            "amount": "0"
                        },
                        {
                            "name": "$20",
                            "count": "0",
                            "amount": "0"
                        },
                        {
                            "name": "$10",
                            "count": "0",
                            "amount": "0"
                        },
                        {
                            "name": "$5",
                            "count": "0",
                            "amount": "0"
                        },
                        {
                            "name": "$1",
                            "count": "0",
                            "amount": "0"
                        }
                    ]
                },
                "coinDetails": {
                    "cashCoinDetailType": [
                        {
                            "name": "$2",
                            "count": "0",
                            "roll": "0",
                            "amount": "0"
                        },
                        {
                            "name": "$1",
                            "count": "0",
                            "roll": "0",
                            "amount": "0"
                        },
                        {
                            "name": "$0.25",
                            "count": "0",
                            "roll": "0",
                            "amount": "0"
                        },
                        {
                            "name": "$0.1",
                            "count": "0",
                            "roll": "0",
                            "amount": "0"
                        },
                        {
                            "name": "$0.05",
                            "count": "0",
                            "roll": "0",
                            "amount": "0"
                        },
                        {
                            "name": "$0.01",
                            "count": "0",
                            "roll": "0",
                            "amount": "0"
                        }
                    ]
                }
            }
        }
        resBalanceCashIn = self.client.service.BalanceCashDrawer(passcode=cashIn_param['passcode'], userAuth=userAuth,
                                                                 type=cashIn_param['type'],
                                                                 amount=cashIn_param['amount'])
        ParsedRespBalanceCashIn = fastest_object_to_dict(resBalanceCashIn)
        self.logger.info(ParsedRespBalanceCashIn)
        self.assertEqual(True, ParsedRespBalanceCashIn['Result']['successful'])

        #Cash out with wrong amount
        # BalanceCashDrawer 获取type 是Cash_in 还是cash_out
        resBalanceCashDrawer = self.client.service.BalanceCashDrawer(passcode="11", userAuth=userAuth)
        ParsedRespBalanceCashDrawer = fastest_object_to_dict(resBalanceCashDrawer)
        self.logger.info(ParsedRespBalanceCashDrawer)
        self.assertEqual(True, ParsedRespBalanceCashDrawer['Result']['successful'])
        self.assertEqual('CASH_OUT', ParsedRespBalanceCashDrawer['expectedType'])
        totalCashSale = ParsedRespBalanceCashDrawer['totalCashSale']

        # BalanceCashDrawer 现金结算
        cashOut_param = {
            "passcode": "11",
            "type": "CASH_OUT",
            "amount": "110",
            "cashDetailType": {
                "noteDetails": {
                    "cashNoteDetailType": [
                        {
                            "name": "$100",
                            "count": "1",
                            "amount": "100"
                        },
                        {
                            "name": "$50",
                            "count": "0",
                            "amount": "0"
                        },
                        {
                            "name": "$20",
                            "count": "0",
                            "amount": "0"
                        },
                        {
                            "name": "$10",
                            "count": "1",
                            "amount": "10"
                        },
                        {
                            "name": "$5",
                            "count": "0",
                            "amount": "0"
                        },
                        {
                            "name": "$1",
                            "count": "0",
                            "amount": "0"
                        }
                    ]
                },
                "coinDetails": {
                    "cashCoinDetailType": [
                        {
                            "name": "$2",
                            "count": "0",
                            "roll": "0",
                            "amount": "0"
                        },
                        {
                            "name": "$1",
                            "count": "0",
                            "roll": "0",
                            "amount": "0"
                        },
                        {
                            "name": "$0.25",
                            "count": "0",
                            "roll": "0",
                            "amount": "0"
                        },
                        {
                            "name": "$0.1",
                            "count": "0",
                            "roll": "0",
                            "amount": "0"
                        },
                        {
                            "name": "$0.05",
                            "count": "0",
                            "roll": "0",
                            "amount": "0"
                        },
                        {
                            "name": "$0.01",
                            "count": "0",
                            "roll": "0",
                            "amount": "0"
                        }
                    ]
                }
            },
        }
        resBalanceCashOut = self.client.service.BalanceCashDrawer(passcode=cashOut_param['passcode'], userAuth=userAuth,
                                                                  type=cashOut_param['type'],
                                                                  amount=cashOut_param['amount'])
        ParsedRespBalanceCashOut = fastest_object_to_dict(resBalanceCashOut)
        self.logger.info(ParsedRespBalanceCashOut)
        self.assertEqual(False, ParsedRespBalanceCashOut['Result']['successful'])

    def test_cashIn_without_authority(self):
        """
        cash In 现金备款账号无权限
        :return:
        """

        # pullCashDrawer 开钱箱
        userAuth = {"userId": 76, "sessionKey": self.sessionKey}

        resPullCashDrawer = self.client.service.PullCashDrawer(userID="76", userAuth=userAuth)
        ParsedRespPullCashDrawer = fastest_object_to_dict(resPullCashDrawer)
        self.logger.info(ParsedRespPullCashDrawer)
        self.assertEqual(True, ParsedRespPullCashDrawer['successful'])

        # BalanceCashDrawer 获取type 是Cash_in 还是cash_out
        resBalanceCashDrawer = self.client.service.BalanceCashDrawer(passcode="22", userAuth=userAuth)
        ParsedRespBalanceCashDrawer = fastest_object_to_dict(resBalanceCashDrawer)
        self.logger.info(ParsedRespBalanceCashDrawer)
        self.assertEqual(True, ParsedRespBalanceCashDrawer['Result']['successful'])
        self.assertEqual('CASH_IN', ParsedRespBalanceCashDrawer['expectedType'])
        # BalanceCashDrawer 往钱箱放钱
        cashIn_param = {
            "passcode": "22",
            "type": "CASH_IN",
            "amount": "100",
            "cashDetailType": {
                "noteDetails": {
                    "cashNoteDetailType": [
                        {
                            "name": "$100",
                            "count": "1",
                            "amount": "100"
                        },
                        {
                            "name": "$50",
                            "count": "0",
                            "amount": "0"
                        },
                        {
                            "name": "$20",
                            "count": "0",
                            "amount": "0"
                        },
                        {
                            "name": "$10",
                            "count": "0",
                            "amount": "0"
                        },
                        {
                            "name": "$5",
                            "count": "0",
                            "amount": "0"
                        },
                        {
                            "name": "$1",
                            "count": "0",
                            "amount": "0"
                        }
                    ]
                },
                "coinDetails": {
                    "cashCoinDetailType": [
                        {
                            "name": "$2",
                            "count": "0",
                            "roll": "0",
                            "amount": "0"
                        },
                        {
                            "name": "$1",
                            "count": "0",
                            "roll": "0",
                            "amount": "0"
                        },
                        {
                            "name": "$0.25",
                            "count": "0",
                            "roll": "0",
                            "amount": "0"
                        },
                        {
                            "name": "$0.1",
                            "count": "0",
                            "roll": "0",
                            "amount": "0"
                        },
                        {
                            "name": "$0.05",
                            "count": "0",
                            "roll": "0",
                            "amount": "0"
                        },
                        {
                            "name": "$0.01",
                            "count": "0",
                            "roll": "0",
                            "amount": "0"
                        }
                    ]
                }
            }
        }
        resBalanceCashIn = self.client.service.BalanceCashDrawer(passcode=cashIn_param['passcode'], userAuth=userAuth,
                                                                 type=cashIn_param['type'],
                                                                 amount=cashIn_param['amount'])
        ParsedRespBalanceCashIn = fastest_object_to_dict(resBalanceCashIn)
        self.logger.info(ParsedRespBalanceCashIn)
        self.assertEqual(False, ParsedRespBalanceCashIn['Result']['successful'])

    def test_cashOut_without_authority(self):
        """
        cash Out 现金结账账号无权限
        :return:
        """

        # pullCashDrawer 开钱箱
        userAuth = {"userId": 76, "sessionKey": self.sessionKey}

        resPullCashDrawer = self.client.service.PullCashDrawer(userID="76", userAuth=userAuth)
        ParsedRespPullCashDrawer = fastest_object_to_dict(resPullCashDrawer)
        self.logger.info(ParsedRespPullCashDrawer)
        self.assertEqual(True, ParsedRespPullCashDrawer['successful'])

        # BalanceCashDrawer 获取type 是Cash_in 还是cash_out
        resBalanceCashDrawer = self.client.service.BalanceCashDrawer(passcode="22", userAuth=userAuth)
        ParsedRespBalanceCashDrawer = fastest_object_to_dict(resBalanceCashDrawer)
        self.logger.info(ParsedRespBalanceCashDrawer)
        self.assertEqual(True, ParsedRespBalanceCashDrawer['Result']['successful'])
        self.assertEqual('CASH_OUT', ParsedRespBalanceCashDrawer['expectedType'])
        # BalanceCashDrawer 往钱箱放钱
        cashIn_param = {
            "passcode": "22",
            "type": "CASH_OUT",
            "amount": "100",
            "cashDetailType": {
                "noteDetails": {
                    "cashNoteDetailType": [
                        {
                            "name": "$100",
                            "count": "1",
                            "amount": "100"
                        },
                        {
                            "name": "$50",
                            "count": "0",
                            "amount": "0"
                        },
                        {
                            "name": "$20",
                            "count": "0",
                            "amount": "0"
                        },
                        {
                            "name": "$10",
                            "count": "0",
                            "amount": "0"
                        },
                        {
                            "name": "$5",
                            "count": "0",
                            "amount": "0"
                        },
                        {
                            "name": "$1",
                            "count": "0",
                            "amount": "0"
                        }
                    ]
                },
                "coinDetails": {
                    "cashCoinDetailType": [
                        {
                            "name": "$2",
                            "count": "0",
                            "roll": "0",
                            "amount": "0"
                        },
                        {
                            "name": "$1",
                            "count": "0",
                            "roll": "0",
                            "amount": "0"
                        },
                        {
                            "name": "$0.25",
                            "count": "0",
                            "roll": "0",
                            "amount": "0"
                        },
                        {
                            "name": "$0.1",
                            "count": "0",
                            "roll": "0",
                            "amount": "0"
                        },
                        {
                            "name": "$0.05",
                            "count": "0",
                            "roll": "0",
                            "amount": "0"
                        },
                        {
                            "name": "$0.01",
                            "count": "0",
                            "roll": "0",
                            "amount": "0"
                        }
                    ]
                }
            }
        }
        resBalanceCashIn = self.client.service.BalanceCashDrawer(passcode=cashIn_param['passcode'], userAuth=userAuth,
                                                                 type=cashIn_param['type'],
                                                                 amount=cashIn_param['amount'])
        ParsedRespBalanceCashIn = fastest_object_to_dict(resBalanceCashIn)
        self.logger.info(ParsedRespBalanceCashIn)
        self.assertEqual(False, ParsedRespBalanceCashIn['Result']['successful'])
