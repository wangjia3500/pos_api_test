#coding=utf-8
import unittest
import datetime
import time
from parameterized import parameterized, param
from common.requests_handler import RequestsHandler
from common.log import Logger
from common.common import fastest_object_to_dict
from suds.client import Client

class CheckTest(unittest.TestCase):
    def setUp(self):
        # 请求类实例化
        self.req = RequestsHandler()
        self.logger = Logger().get_log
        # 登录
        login_url = 'http://localhost:22080/kpos/webapp/license/clientInstanceLogin'
        payload = {
            "appInstanceName": "YY",
            "appInstanceType": "POS"
        }

        res = self.req.visit('post', login_url, json=payload)
        self.sessionKey = res['sessionKey']

        #员工登录
        dest_url = 'http://localhost:22080/kpos/ws/kposService?wsdl'
        headers ={"Connection": "keep-alive"}
        self.client = Client(dest_url)
        self.client.set_options(headers=headers)

        createuserApi = {"userAuth": {"userPasscode": "11", "sessionKey": self.sessionKey},}
        userAuth_temp = createuserApi["userAuth"]

        res = self.client.service.ListPrivileges(passcode="11", fetchClockInOutStatus=True, userAuth=userAuth_temp)
        ParsedResponse = fastest_object_to_dict(res)
        self.assertEqual(True, ParsedResponse['Result']['successful'])

    def tearDown(self):
        # 关闭连接
        self.req.close_session()

    def test_getStaff(self):
        """
        获取员工的信息
        :return:  id，name, wage, wageType, status, lastActiveTimeStamp, requireClockInout,
        requireCashInOut, attendanceRecords
        """

        resGetStaff = self.client.service.GetStaff(userId=1)
        ParsedResponseGetStaff = fastest_object_to_dict(resGetStaff)
        self.logger.info(ParsedResponseGetStaff)
        self.assertEqual(True,ParsedResponseGetStaff['Result']['successful'])

    @parameterized.expand([
        param("IN"),
        param("START_BREAK"),
        param("END_BREAK"),
        param("OUT")
    ])
    def test_staffCard(self, status):
        """
        员工登录登出或者休息
        :return: status
        """
        resStaffCard = self.client.service.StaffCard(passcode="11", cardType=status, roleId=1)
        ParsedResponseStaffCard = fastest_object_to_dict(resStaffCard)
        self.logger.info(ParsedResponseStaffCard)
        self.assertEqual(True,ParsedResponseStaffCard['Result']['successful'])
        if status == 'START_BREAK':
            status = 'BREAK_STARTED'
        if status == 'END_BREAK':
            status = 'IN'

        self.assertEqual(status, ParsedResponseStaffCard['status'])

    def test_add_staff_breakTime(self):
        """
        checkIn checkOut 之间添加休息时间
        :return:
        """
        # check In
        resStaffCard = self.client.service.StaffCard(passcode="11", cardType="IN", roleId=1)
        ParsedResponseStaffCard = fastest_object_to_dict(resStaffCard)
        self.logger.info(ParsedResponseStaffCard)
        self.assertEqual(True,ParsedResponseStaffCard['Result']['successful'])
        self.assertEqual("IN", ParsedResponseStaffCard['status'])

        # fetchAttendance
        today = datetime.date.today()
        resFecthAttendance = self.client.service.FetchAttendance(fromDate=today, toDate=today)
        responFetchAttendance = fastest_object_to_dict(resFecthAttendance)
        self.logger.info(responFetchAttendance)
        self.assertEqual(True, responFetchAttendance['Result']['successful'])
        print(len(resFecthAttendance['attendance']))
        index = len(resFecthAttendance['attendance']) - 1
        attendanceId = resFecthAttendance['attendance'][index]['id']

        # 添加休息时间
        startTime = datetime.datetime.now()
        endTime = startTime + datetime.timedelta(minutes=1)

        staffBreak = {
            "startTime": startTime,
            "endTime": endTime,
            "staffId": "1",
            "attendanceId": attendanceId
        }
        userAuth = {"userId": 1}

        resSaveStaffBreak = self.client.service.SaveStaffBreak(staffBreak=staffBreak, userAuth=userAuth)
        responSaveStaffBreak = fastest_object_to_dict(resSaveStaffBreak)
        self.logger.info(responSaveStaffBreak)
        self.assertEqual(True, responSaveStaffBreak['successful'])

        time.sleep(90)

        # checkout
        resCheckout= self.client.service.StaffCard(passcode="11", cardType="OUT", roleId=1)
        responCheckout = fastest_object_to_dict(resCheckout)
        self.logger.info(responCheckout)
        self.assertEqual(True,responCheckout['Result']['successful'])
        self.assertEqual("OUT", responCheckout['status'])

    def test_add_staff_endTime(self):
        # check In with Manage role
        resStaffCard = self.client.service.StaffCard(passcode="11", cardType="IN", roleId=2)
        ParsedResponseStaffCard = fastest_object_to_dict(resStaffCard)
        self.logger.info(ParsedResponseStaffCard)
        self.assertEqual(True, ParsedResponseStaffCard['Result']['successful'])
        self.assertEqual("IN", ParsedResponseStaffCard['status'])

        # fetchAttendance
        today = datetime.date.today()
        resFecthAttendance = self.client.service.FetchAttendance(fromDate=today, toDate=today)
        responFetchAttendance = fastest_object_to_dict(resFecthAttendance)
        self.logger.info(responFetchAttendance)
        self.assertEqual(True, responFetchAttendance['Result']['successful'])
        print(len(resFecthAttendance['attendance']))
        index = len(resFecthAttendance['attendance']) - 1
        attendanceId = resFecthAttendance['attendance'][index]['id']
        startTime = resFecthAttendance['attendance'][index]['startTime']

        time.sleep(90)
        # checkout
        attendance = {
          "id": attendanceId,
          "staffId": "1",
          "startTime": startTime,
          "endTime": datetime.datetime.now(),
          "roleId": "2"
        }
        userAuth = {"userId": 1}
        resCheckout= self.client.service.UpdateAttendance(attendance=attendance, userAuth=userAuth)
        responCheckout = fastest_object_to_dict(resCheckout)
        self.logger.info(responCheckout)
        self.assertEqual(True,responCheckout['successful'])
